import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-book',
    template: `
        <h3>Thêm sách</h3>
        <div class="form-group">
            <label>Tên sách:</label>
            <input class="form-control" type="text" [(ngModel)]="bookName" />
        </div>
        <div class="form-group">
            <label>Tên sách:</label>
            <input class="form-control" type="text" [(ngModel)]="bookAuthor" />
        </div>
        <div class="form-group">
            <label>Hình:</label>
            <input class="form-control" type="text" [(ngModel)]="bookUrlImage" />
        </div>
        <div class="form-group">
            <label>Loại sách:</label>
            <select class="form-control" [(ngModel)]="bookType">
                <option>Tất cả</option>
                <option value="tu-duy-ky-nang-song">Sách tư duy - kỹ năng sống</option>
                <option value="ky-nang-song">Kỹ năng sống</option>
            </select>
        </div>
        <button class="btn btn-primary">Thêm</button>
        <hr/>
        <h3>Phân loại sách</h3>
        <div class="form-group">
            <select class="form-control">
                <option>Tất cả</option>
                <option value="tu-duy-ky-nang-song">Sách tư duy - kỹ năng sống</option>
                <option value="ky-nang-song">Kỹ năng sống</option>
            </select>
        </div>
        <hr/>
        <h3>Danh sách</h3>
        <div class="books">
            <div class="row">
                <div class="col-lg-4" *ngFor="let b of listBooks">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ b.image }}" alt="{{ b.name }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ b.name }}</h5>
                            <p class="card-text">{{ b.author }}</p>
                            <p><span class="badge badge-secondary">{{ b.typeLabel }}</span></p>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-success" *ngIf='b.isRead' (click)='updateStatus(b);'>Đã dọc</button>
                                <button type="button" class="btn btn-warning" *ngIf='!b.isRead' (click)='updateStatus(b);'>Chưa đọc</button>
                                <button type="button" class="btn btn-danger" (click)='removeBook(b.id);'>Xóa</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class BookComponent {
    bookName = '';
    bookAuthor = '';
    bookType = '';
    bookUrlImage = '';

    listBooks = [
        {
            'id': 1,
            'name': 'Mình Nói Gì Khi Nói Về Hạnh Phúc?',
            'author': 'Rosie Nguyễn',
            'isRead': true,
            'typeLabel': 'Sách tư duy - kỹ năng sống',
            'type': 'tu-duy-ky-nang-song',
            'image': 'https://vcdn.tikicdn.com/cache/550x550/ts/product/5a/63/29/c805950c434394a8f9df3223af604e40.jpg'
        },
        {
            'id': 2,
            'name': 'Tuổi Trẻ Đáng Giá Bao Nhiêu',
            'author': 'Rosie Nguyễn',
            'isRead': false,
            'typeLabel': 'Sách kỹ năng sống',
            'type': 'ky-nang-song',
            'image': 'https://vcdn.tikicdn.com/cache/550x550/media/catalog/product/t/u/tuoi-tre-dang-gia-bao-nhieu-u547-d20161012-t113832-888179.u3059.d20170616.t095744.390222.jpg'
        }
    ];

    updateStatus(b) {
        // tim sach co id = id
        // const b = this.listBooks.find(book => book.id === idBook);

        b.isRead = !b.isRead;
    }

    removeBook(idBook) {
        // const b = this.listBooks.find(book => book.id === idBook);
        const index = this.listBooks.findIndex(b => b.id === idBook);
        
        this.listBooks.splice(index, 1);
    }
}
